---
id: 3d_models
title: 3d models of Auckland heritage buildings.
---
Here are a variety of 3D models which are an extension of the building recording process described previously. 

## Colonial period house ##
The first is of an early colonial period house modelled in Sketchup. This as done directly using the original elevation drawings. The model was uploaded to sketchfab and embedded in this page.

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/e99a426686244e03a3273445fa6549a3/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

<p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;">
    <a href="https://sketchfab.com/models/e99a426686244e03a3273445fa6549a3?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">1870&#39;s nz house</a>
    by <a href="https://sketchfab.com/kelp.strewn?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">kelp.strewn</a>
    on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a>
</p>
</div>

## Bungalows ##
The next 2 houses are bungalows and are based on a pair of houses in Provost Street, Ponsonby. One is a California bungalow. The other is an English bungalow. This typology / style description is according to Jeremy Ashford, see his excellent book [The Bungalow in New Zealand](/img/https://books.google.co.nz/books/about/The_Bungalow_in_New_Zealand.html?id=hk9jGwAACAAJ&redir_esc=y) 

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/9253163a82664090972e58f2a52425d2/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/models/9253163a82664090972e58f2a52425d2?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">California bungalow, exterior</a> by <a href="https://sketchfab.com/kelp.strewn?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">kelp.strewn</a> on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a> </p> </div>

<div class="sketchfab-embed-wrapper"><iframe width="640" height="480" src="https://sketchfab.com/models/314f725168794d348a8f0c738fc0a3d2/embed" frameborder="0" allow="autoplay; fullscreen; vr" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/models/314f725168794d348a8f0c738fc0a3d2?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">English Bungalow</a> by <a href="https://sketchfab.com/kelp.strewn?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">kelp.strewn</a> on <a href="https://sketchfab.com?utm_medium=embed&utm_source=website&utm_campain=share-popup" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a> </p> </div>





