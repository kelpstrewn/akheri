---
id: building_recording
title: Building recording
---
## Introduction
Conservation architects, architectural historians and urban historians are all in in agreement about the recording of the physical fabric of historic buildings. There is an Icomos document devoted to this issue. This is [Principles for the recording of monuments , groups of buildings and sites](/img/https://www.icomos.org/charters/archives-e.pdf) (Icomos 1996 ). This document states that it is important to provide "a permanent record of all monuments, groups of buildings and sites that are to be destroyed or altered in any way, or where at risk from natural events or human activities".
 
An advisory document for building recording in New Zealand is "Investigation and Recording of Buildings and Standing Structures". It is still available from Heritage New Zealand [here](/img/http://www.heritage.org.nz/~/-/media/bd20738fc99e4f69bec8cec6395b251a.ashx). The document is dated 2006 and has been assigned an isbn number, 978-0-908577-91-0 (online). Authorship is not stated, but an archaeologist employed by the trust, Martin Jones, is thought to be largely responsible.

The document describes methods of recording and appropriate levels or intensity of recording from level one to level four.The methods suggested are traditional and paper based. Film photography and the use of 4H pencil on permatrace for the preparation of manual drawings are privileged. This national guidance advisory does not advise with regard to the fate of the data gathered, and who the data should be accessed by.

## Criticism
There has been criticism from people working in New Zealand as conservation architects and building recorders of "Investigation and Recording of Buildings and Standing Structures". This criticism is centred around a lack of clarity about the levels stated, and what they mean, as the levels are defined by the addition of words such as extensive and comprehensive as the level of significance rises. These words are not further defined within the document. The lack of acknowledgement of the pervasiveness of digital methods in building recording now, and in architectural practice here, and the lack of clear guidance around the maintenance of archives, and of access of the interested public to building records are also regarded as significant detriments to the value of this document.
## English practice
Another advisory document, from an overseas jurisdiction, is "Understanding Historic Buildings, a Guide to Good Recording Practice", English Heritage, 2006, 2016. [This document](/img/https://content.historicengland.org.uk/images-books/publications/understanding-historic-buildings/heag099-understanding-historic-buildings.pdf/) was 18 pages long in 2006, and the new version is 64 pages long. It credits Rebecca Lane for the text, based on the earlier text of Adam Menuge. Digital photography and rectified digital photography are mentioned. Modern digital techniques such as Lidar 3d scanning are mentioned. Levels of record are better described, in comparison with the Heritage NZ document, and the point is made the different levels may be applied to different parts of buildings. A number of paragraphs relating to archiving and access appear. The document is accompanied by example drawings.
## The Getty
In 2007 the Getty produced "Recording, Documentation, and Information Management for the Conservation of Heritage Places." This is a book length document, and is available as either a two volume printed book, or online. The Getty says of itself:- 
> "The Getty Conservation Institute works to advance conservation practice in the visual arts, broadly interpreted to include objects, collections, architecture, and sites. It serves the conservation community through scientific research, education and training, model field projects, and the broad dissemination of the results of both its own work and the work of others in the field. In all its endeavours, the Conservation Institute focuses on the creation and dissemination of knowledge that will benefit the professionals and organizations responsible for the conservation of the world's cultural heritage." 

The Getty is located in Los Angeles and is privately funded through bequests from Jean Paul Getty who was rumoured to be the richest man in the world in 1957. He was born in 1892 in Minneapolis and raised as an only child. He entered the oil industry and became a millionaire by the time he was 23. By the 1960s Getty Oil was one of the largest oil companies in the world.

## Best practice, the Getty recommends:-
The book's editorial review board included people from a variety of reputable international heritage bodies. The author, Robin Letellier, had particular expertise in architectural photogrammetry. The book is written with a building conservation focus rather than an archaeological focus. Sir Bernard Feilden, a conservation architect is quoted, and Feilden's view, that the purpose of conservation is to focus attention on both slowing the rate of decay of buildings and enhancing the artistic and historic messages of heritage places and buildings to the greatest degree possible in a specific context, is endorsed. The first volume of the book is 174 pages long and contains guiding principles for heritage information management, documentation and recording. 

The second volume is 165 pages long and contains complete worked examples of heritage recording and  archiving in particular contexts. This is in line with the premise given in the guiding principles, that all heritage recording is best carried out on a case by case basis. Methods adopted must be adapted to suit the structure or site in question, the available technologies, and the skill levels of those available to implement the available technologies. Some of the worked examples given are archaeological but most are of in-use buildings and structures.
 


The book places stress on the storage and archiving of the information gathered. Digital methods are favoured.

>"Today, computer-based information technologies allow the digitizing of traditional inventories in order to make data more easily accessible and make enhanced queries possible.
>Moreover, digital inventories, which are able to manage large quantities of text and image data, facilitate the addition of special categories, providing detailed information on topics such as historical development, original materials and techniques, use, condition, and the inclusion of image data."
>“Recording, Documentation, and Information Management for the Conservation of Heritage Places “ pg 45

The level of recording detail is simplified from the four levels suggested by English heritage and the NZHPT. There are 3 levels, and a longish quote that describes the levels in detail follows.

>“Level 1: Reconnaissance Recording
>Reconnaissance recording is typically an overview photo survey with sketched plans that allows conservation professionals to visualize, in entirety, a site and its related buildings and features in sufficient detail to understand the site’s overall characteristics. It should permit rapid identification of significant features and problem areas. The quantity of photos taken will vary according to the size of the site and related structures and features, as well as the client’s requirements. For a building, reconnaissance recording normally would include sketches of plans and elevations together with important details. More complex sites such as cultural landscapes or archaeological excavations will require general views from all compass points and at various height elevations (i.e., heights of land), supplemented, as needs dictate, by representative details.

>Level 2: Preliminary Recording
>Preliminary recording is more accurate than reconnaissance recording and includes measured graphic records. It is meant to complement reconnaissance recording by providing more complete information pertaining to significant components of a site. The purpose of this recording is to produce a set of graphic records of the asset’s major features that are needed early in the conservation process for preliminary analysis, and to define areas for further investigation and related detailed recording. The accuracy of graphic records is ± 10 cm for plans, elevations, and cross sections, and ± 2 cm for structural and other elements.

>Level 3: Detailed recording, which is generally the most accurate level of heritage recording possible, may take place prior to, during, or after a conservation activity to accurately record a site’s physical configuration, condition, and significant features. Detailed recording occurs when a highly significant resource becomes the subject of directed research and analysis or of intervention planning and conceptual design.
>To ensure cost-effective detailed recording, completeness should be tailored to the immediate needs of a conservation team. Detailed recording may be phased over a number of years depending on planning requirements and related budget. The accuracy of a detailed record can vary between approximately ± 2 mm and 5 mm for building elements and between ± 10 mm and 25 mm for building plans, elevations, and cross sections.

>One of the main differences between the three levels of recording is the accuracy of graphic data produced. The higher the accuracy, the more time needed to record, and/or the more sophisticated recording tools are needed. This translates into higher costs to produce the record.”>

>“Recording, Documentation, and Information Management for the Conservation of Heritage Places” pg 37

As previously said recording methods are on a case by case basis and can be either digital or hand measured. The document and examples do devote considerable space to digital methods though, and there is an implication that in advanced high wage economies the speed of digital methods will win out over slower low tech labour intensive hand measurement. 

Having reviewed the three documents described above I conclude that  "Recording, Documentation, and Information Management for the Conservation of Heritage Places" remains the best reference work when considering the design of a built heritage documentation programme aimed at conserving a record of the geometry and materials of buildings that are to be demolished. The 2016 English document is also very useful.
 
## Suggestions regarding techniques and processes for recording in New Zealand.
This draws on my experience as an architect and conservation architect in recording existing building fabric. I will combine this with information I have obtained from a careful reading of the three advisory documents previously discussed, and with information gained from discussion with other practitioners.

Twenty five years ago methods in architecture were all manual. Drawings were produced on a drawing board, and site measurements were made using tape measures, set squares, plumb bobs and optical levels of various types.

Now, architectural practice is almost universally digital. Drawings are made using cad systems on computers. Site measurements are made using hand held laser measuring devices such as the Leica Disto. Total station (electronic theodolite (transit) integrated with an electronic distance meter)  surveys have the ability to quickly and accurately record both topographic and building surveys. 3d scanning using lidar has progressed in leaps and bounds and is now on the cusp of becoming a reasonably priced and widely accessible technology. There have also been remarkable advances in digital photogrammetry techniques.

Thus a version of a summary chart of techniques and levels might look like this.


![Chart](/img/building_rec01.jpg)
In reality methodologies are rarely as clear cut as the table suggests. Total station survey data may be combined with manually measured dimensions, placed on site sketches and then later combined on a cad workstation. One of the examples from Recording, Documentation, and Information Management for the Conservation of Heritage Places is of the recording of a military fort in Canada. A combination of methods was used in this case. 


Manual recording techniques (hand measuring) are possible and will be appropriate to small simple buildings that are close to the ground. Some equipment will be need  to be purchased. Drawings resulting from such recording will in the first instance be hand sketches. These can be made formal in cad, where the level of recording justifies the work involved. 

For more complex building geometries and higher levels of accuracy the use of total station survey and rectified digital photography is recommended. This will require the purchase of the necessary equipment and software. Training in the use of the equipment will also be needed.

For a higher level of record involving complex and inaccessible geometry or where 3d textured models are required for interpretation reasons there are two alternative approaches.

## Structure from motion
Structure from motion (sfm) is a technique that is a development of photogrammery. Multiple photographs from different viewpoints are taken. These are taken into a digital environment and software is used to automatically calculate the viewpoint from which each photo was taken. The software then matches points from photo to photo, and use this information to calculate the 3d coordinates of each point. The software is then able to make a 3d mesh based on the point cloud and to apply a texture to the mesh.

<div class="flex-container">
<div style="display: block; width: 360px; float:left; padding:20px;" >
<figure>
  <p><img src="/img/building_rec02.jpg">
  <figcaption>Elevation of small rural church, reconstructed from photographs</a></figcaption>
</figure>
</div>
</div>

The resulting model is scale indeterminate. It is then imported into 3d mesh manipulation software such as cloudcomparee or even sketchup. Here, the model can be scaled, according to reference (or "control") measurements made on site. 

 The heart of this system is software based. Because of the high computing demands of the software there are  'cloud' based software solutions. Over the last five years I have recorded a number of buildings and monuments using cloud based solutions such as; Autodesk 123d Catch, (discontinued several years ago) Autodesk Recap, (discontinued recently) and local pc based solutions such as Colmap, 3df Zephyr and Agisoft Photoscan. 

 I have experimented with different camera types, point and shoot cameras, dslr cameras, and phone cameras. Results were usable in most cases. Accuracy is largely dependent on the quality and number of images used. The use of a large sensor dslr camera off a tripod and in ideal overcast light conditions gives the best results.  A reasonably high spec computer with cuda cores speeds the process greatly.

Poor results are obtained when there are difficulties getting multiple unobscured shots of the subject building. This is the case when there are other buildings or objects in the foreground, or when obscuring vegetation is present. Buildings that are built of very uniform and slightly reflective material also return poor results as the software has difficulty in fixing points between photographs.
 
## Lidar scanning
Another method is 3d scanning, lidar based. Lidar is a remote sensing technology that measures distance by illuminating a target with a laser and analyzing the reflected light. The term lidar is  a portmanteau of "light" and "radar".

The technique is very accurate, and the scanning process is simple. 3d scanning has found application recently in a much wider range of industries, and because of this the price of scanners is reducing rapidly at present. However, because of cost I do not have access to this equipment, and only have experience of working with point clouds produced by others. 

## Cyark

The state of the art now, with regard to public presentation of building records is to be found in websites produced by organisations such as Cyark.  CyArk was founded in 2003 in response to the Taliban's destruction of the 1600-year-old Bamiyan Buddhas in Afghanistan.  Cyark's goal is to ensure that heritage sites are available to future generations, while making them  accessible to the public online now.  New technologies are being used to create a free, 3D online library of the world's cultural heritage sites. Particular attention is paid to sites that are likely to be  lost to natural disasters, destroyed by human activity or and lack of activity (maintenance). 
[http://www.cyark.org/about/](/img/http://www.cyark.org/about/ "Cyark is here...")

## Conclusions
Institutions around the world are now moving to a web based presentation of their heritage. Such sites often allow for user manipulation of heritage 3d models, textured, or dense coloured point clouds, together with hotspots and links to historic images and commentary, both textual and audio based.

