---
id: drawing_joinery
title: Drawing Elevations, joinery etc
---
## Introduction ##
To get a good building or to alter an existing building well it is important to have good design. A key skill for a good designer to achieve good results is to have control over the draughting process. It is scale drawings that make a project and that underpin the project decision making. Well planned buildings are essential but elevations too are important in achieving a quality result. Here I am mainly focusing on altering and adding to existing buildings.

<div class="flex-container">
<div style="display: block; width: 30%; float:left; padding:20px;" >
<figure>
  <p><img src="/img/drawing_joinery03.jpg">
  <figcaption>Double hung windows in elevation.</a></figcaption>
</figure>
</div>
</div>

Much of what I have said above will be well known to older members of the architecture and draughting professions. However, I have noticed in recent times, with the development of computer methods that are based around 3D computer models, that the quality of the 2D drawings, plans and particularly elevations, is in decline. This effect may be built into some of the software packages such as Revit, where for alterations work the components or families available with the software by default do not relate well to what exists on site. This is pronounced when working with the old houses of Auckland, which do not necessarily have close overseas equivalents. 
 
## Declining quality ##

The development of a new custom family, in software, that better represents the situation is a drawn out and tricky process. Many do not know how to do it or do not have time to do it. In some ways this means that older 2d cad based software packages such as AutoCAD and its clones will be better, more suitable and quicker, for additions and alterations work. That said, I'm pretty agnostic about 3D modelling versus 2d cad. It is the outcome, the drawings and their quality, that is important.

## Drawing a double hung window. ##

Here are some thoughts and rough guidance to the inexperienced on how to achieve better quality in architecture drawing. A crucial first step is very careful measuring and recording of what exists. Taking the example of traditional window joinery here are some suggestions for the process. Record on site such information as:-

1. the size of the opening 
2. the size of the sash and how much of it is visible 
3. the approximate profile of the sill 
4. the width of the architraves will be important. When measuring the house it is good also to record the height of the rough opening at sill level, above floor level.

As you develop your elevations it is important to have in mind the scale at which they will be reproduced and printed, the size of the scaled output when printed at scale. This will have a big influence on how much detail should be including in the drawing. 

The control of lineweight is also important. When multiple lines are very close together a very thin line should be used. The use of medium or heavy line weights around the perimeter of the element may also be a good idea, to give your drawings punch and variety.
 
<div class="flex-container">
<div style="display: block; width: 30%; float:left; padding:20px;" >
<figure>
  <p><img src="/img/drawing_joinery01.jpg">
  <figcaption>Double hung windows.</a></figcaption>
</figure>
</div>
</div>
 
 A PDF of the image, that can be printed to scale can be obtained from the link. There are also dxf links, at the end of this page. 

<a href="/heritage/drawing_joinery/traditional_timber_joinery-double_hung_fancy_architrave.pdf"> double hung pdf </a>

To continue the example I would like to carry on the discussion of an output drawing of a double hung window. These are very common and strongly associated with the timber villas of Auckland built about 120 years ago. In representing one of these following points should be noted. 

1. In elevation the main frame of the window will sit behind the architrave and be concealed from view. 
2. The sash rails and stiles will be partially visible and there is generally a variation in depth between the rails at the bottom and the centre. 
3. A fair bit of detail should be left out at a scale 1:100, such as scribers, putty lines and detail associated with the sill profile.

<figure>
  <p><img src="/img/drawing_joinery02.jpg">
  <figcaption>Casement windows - bungalow period.</a></figcaption>
</figure>

## Good draughting practice generally ##
When I first began in architecture, my grandfather, a successful building contractor with an interest in design, gave me a book he had used in designing a house for himself. This book was [Draughtsmanship](/img/https://books.google.co.nz/books/about/Draughtsmanship.html?id=9fm9QAAACAAJ&redir_esc=y) by Fraser Reekie. I still refer to this on occasion and the principles of good drafting practice given here are still valid. Interestingly, with the rise in the internet a wide variety of traditional drafting instruction books are now available for free. Through the Internet archive these have been scanned, and made available for download, as they are out of copyright. Here are links to some of these. 

---
| book | book |
| ------ | ------ |
|[Modern Drafting 1911](/img/https://archive.org/details/moderndrafting00mill) | [Architectural Drawing Plates 1919](/img/https://archive.org/details/ElwoodArchDrawingPlates0001)  |
| [Progressive Steps 1919](/img/https://archive.org/details/cu31924074480520) |  [Primer of Architectural Drawing for Young Students 1910](/img/https://archive.org/stream/aprimerarchitec00danagoog#page/n100/mode/2up) |



## Drawings for download ##
  
<figure>
  <p><img src="/img/drawing_joinery04.jpg">
  <figcaption>Paired and multiple double hung windows.</a></figcaption>
</figure>
    
For young architects and designers preparing drawings of old Auckland houses I am providing some pdf files and dxf files for download. These can be used as a starting point when representing a building at the common elevation scale of 1:100. I'll put more of these up in this page, with comments, as I develop them. These are in part measured and drawn by me, and in part redrawn, referring to an old manufacturers catalogue, which is no longer available. 

A further excellent resource, which includes sketch construction detail for traditional windows can be found [here](/img/https://www.renovate.org.nz/villa/windows-doors-other-joinery-and-hardware/windows/), on the Branz Renovate website. 

<a href="/heritage/drawing_joinery/traditional_timber_joinery-double_hung_divided_light.pdf"> double hung divided light pdf </a>

<a href="/heritage/drawing_joinery/traditional_timber_joinery-double_hung_divided_light.dxf"> double hung divided light dxf </a>

<a href="/heritage/drawing_joinery/traditional_timber_joinery-bungalow_casements.pdf"> bungalow casement pdf </a>

<a href="/heritage/drawing_joinery/traditional_timber_joinery-bungalow_casements.dxf"> bungalow casement dxf </a>

<a href="/heritage/drawing_joinery/traditional_timber_joinery-double_hung_pair.pdf"> double hung pair and triple pdf </a>

<a href="/heritage/drawing_joinery/traditional_timber_joinery-double_hung_pair.dxf"> double hung pair and triple dxf </a>


